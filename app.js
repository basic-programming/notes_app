import {addNote, removeNote, getNotes, readNote} from './notes.js';

import yargs from 'yargs';

const log = console.log;

// log(process.argv);
// log(argv);

// Create add yargs.command.......
import {hideBin} from 'yargs/helpers'
yargs(hideBin(process.argv)).command({
    command: 'add',
    describe: 'Add a note',
    builder: {
        title: {
            describe: "Note Title",
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Body of Note',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        addNote(argv.title, argv.body);
    }
}).parse();


// Create remove yargs.command.......
yargs(hideBin(process.argv)).command({
    command: 'remove',
    describe: 'remove a note',
    builder: {
        title: {
            describe: "Note title to be removed",
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        removeNote(argv.title);
    }
}).parse();

// // Create list yargs.command.......
yargs(hideBin(process.argv)).command({
    command: 'list', 
    describe: 'list put all notes',
    handler() {
        getNotes();
    }
}).parse();

// // Create a read yargs.command.....
yargs(hideBin(process.argv)).command({
    command: 'read',
    describe: 'Reads a note',
    builder: {
        title: {
            describe: 'Note title you want to read',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        readNote(argv.title);
    }
}).parse();


// it is important beacause without this yargs doesnot working as expected.....
// yargs.parse();
import chalk from 'chalk'; 
const log = console.log;
import fs from 'fs';
import { title } from 'process';

const readNote = (argTitle) => {
    const notes = loadNotes();
    const note = notes.find(note => note.title === argTitle);
    if (note) {
        log(chalk.blue.inverse("\n ==> " + note.title + "       "));
        log(chalk.inverse(" ==> " + note.body + "       \n"));
    } else {
        log(chalk.red.inverse("\n ==> Sorry no matching note found........\n"));
    }
}

const getNotes = () => {
    const notes = loadNotes();
    log(chalk.inverse("\n ==> Your Notes..........."));
    notes.forEach(element => {
        log(chalk.blue.inverse(" ==> " + element.title + "       "));
    });
    log();
    
}

const removeNote = (argTitle) => {
    const notes = loadNotes();
    const matchNote = notes.find(note => note.title === argTitle);
    if (matchNote) {
        notes.splice(notes.indexOf(argTitle), 1);
        saveNotes(notes);
        log(chalk.green.inverse("Note removed successfully........"));
    } else {
        log(chalk.red.inverse("No matching note........"));
    }
}

const addNote = (argTitle, argBody) => {
    const notes = loadNotes();
    const duplicateNote = notes.find(note => note.title === argTitle);
    if (!(duplicateNote)) {
        notes.push({
            title: argTitle,
            body: argBody
        });
        saveNotes(notes);
        log(chalk.green.inverse("Note added......"));
    } else {
        log(chalk.red.inverse("Note Title Already used......"));
    }
}


const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes);
    fs.writeFileSync('notes.json', dataJSON);
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json');
        return JSON.parse(dataBuffer);
    } catch (e) {
        return [];
    }
}

export {
    getNotes,
    addNote,
    removeNote,
    readNote
}